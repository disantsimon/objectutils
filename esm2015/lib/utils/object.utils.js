/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} object
 * @param {?=} replacer
 * @param {?=} space
 * @return {?}
 */
export function toString(object, replacer, space) {
    return JSON.stringify(object, replacer, space);
}
/**
 * used to get a clone of an object and prevent access by ref,
 * if type is specified object will ereditate al function of the type
 *
 * class Person{
 *   public name:string;
 *   constructor(name){
 *      this.name = name;
 *   }
 *
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let jon = new Person('JON');
 * let anotherJon = clone<Person>(jon,Person);
 *
 * @template T
 * @param {?} object source object to be cloned
 * @param {?=} type type if you want to cast the object and get all function of that
 * @return {?}
 */
export function clone(object, type) {
    if (object) {
        /** @type {?} */
        let tmp = JSON.parse(JSON.stringify(object));
        return castToObject(tmp, type || object.constructor || Object);
    }
    return object;
}
/**
 * set the value at path of a object, if is given constructor or a type, the setted value will be casted
 *
 * es
 *
 * class Car{
 *
 *     public owner:Person;
 * }
 *
 * class Person{
 *
 *   public name:string;
 *
 *   constructor(name){
 *      this.name = name;
 *   }
 *
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let obj = {};
 *
 * SetValueByPath(obj,'owner',new Person('JON'))
 * obj=> { owner : { name:'JON', getName:fn }}
 *
 * @template T, T2
 * @param {?} object object that will be manipulated
 * @param {?} path path of property to assign  value
 * @param {?} value
 * @param {?=} constructor constructor, class type, for cast object
 * @return {?}
 */
export function SetValueByPath(object, path, value, constructor) {
    if (path == null)
        return object;
    if (object == null) {
        object = (/** @type {?} */ ({}));
    }
    /** @type {?} */
    let _path = path.replace(/\[(\w+)\]/g, ".[$1]").split(".").filter(f => f != "");
    /** @type {?} */
    let tmp = object;
    for (let i = 0; i < _path.length; i++) {
        /** @type {?} */
        let key = _path[i];
        if (key.match(/\[(\w+)\]/g) != null) {
            key = key.replace(/\[(\w+)\]/g, "$1");
        }
        /** @type {?} */
        let isArray = function () {
            if (i + 1 < _path.length) {
                return _path[i + 1].match(/\[(\w+)\]/g) != null;
            }
            return false;
        }();
        if (tmp[key] && i != _path.length - 1)
            tmp = tmp[key];
        else if (i != _path.length - 1) {
            tmp[key] = isArray ? tmp[key] || new Array() : tmp[key] || {};
            tmp = tmp[key];
        }
        else {
            if (constructor && value) {
                value = castToObject(value, constructor);
            }
            tmp[key] = value;
        }
    }
    return object;
}
/**
 * By a given object, this function return the property specified by the path
 *
 * let obj = {
 *      a : 2,
 *      c : [{
 *              a:2
 *          },2,
 *          {c :
 *              {
 *                  c : 2
 *              }
 *           ]
 *          }
 *
 * let value = GetValueByPath(obj,'a');
 * value=>2
 * let value = GetValueByPath(obj,'c');
 * value => [...]
 * let value = GetValueByPath(obj,'c[0].a');
 * value => 2
 * @template T
 * @param {?} object object that contain value
 * @param {?} path full path of object
 * @return {?}
 */
export function GetValueByPath(object, path) {
    if (path == null)
        return object;
    /** @type {?} */
    let _path = path.replace(/\[(\w+)\]/g, ".$1").split(".").filter(f => f != "");
    /** @type {?} */
    let res = object;
    for (let k of _path) {
        if (!res)
            return res;
        res = res[k];
    }
    return res;
}
/**
 * assign all property of element to a returned object, if assignTo is given
 * property would be setted to assignTo object
 *
 * es:
 *
 *
 * class Person{
 *   public name:string;
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let element = { name:'Jon'};
 *
 * element does not have property function getName(), element.getName() generate exception
 *
 * let person = assign<Person>(element,Person);
 *
 * person will be an object typed as Person, and getName will be setted correctly and return 'Jon'
 *
 * es.2
 *
 * let element = { name:'Jon'};
 *
 * let person:Person;
 *
 * assign<Person>(element,Person,person);
 *
 * person will be an object typed as Person, and getName will be setted correctly and return 'Jon'
 *
 * @template T
 * @param {?} element source of data
 * @param {?} type type of result, used to cast object returned
 * @param {?=} assignTo used to set property of element to another object
 * @return {?}
 */
export function assign(element, type, assignTo) {
    /** @type {?} */
    let res = assignTo;
    res = castToObject(element, type, assignTo || res);
    return res;
}
/**
 * decorator to set a type of a object property
 * this would be useful to cast object element of an array cause Object.assign don't work
 *
 * @template T
 * @param {?} type type definition of object
 * @param {?=} defaultValue set a default value to object
 * @return {?}
 */
export function setType(type, defaultValue) {
    return function (target, propertyName) {
        /** @type {?} */
        var opt = {
            value: type,
            enumerable: false
        };
        Object.defineProperty(target, "constructor_" + propertyName, opt);
        opt = {
            value: target[propertyName] || defaultValue || new ((/** @type {?} */ (type)))(),
            writable: true
        };
        Object.defineProperty(target, propertyName, opt);
        opt = {
            get: function () {
                return type;
            },
            enumerable: false
        };
        Object.defineProperty(target[propertyName], "type", opt);
    };
}
/**
 * return  a T object casted from params type
 * @template T
 * @param {?} data object to cast (source)
 * @param {?} type typeof (constructor) result
 * @param {?=} assignTo optional, if you would assign result to existing object
 * @return {?}
 */
export function castToObject(data, type, assignTo) {
    /** @type {?} */
    var sType = function (target, type, propertyName) {
        /** @type {?} */
        var opt = {
            get: function () {
                return type;
            },
            enumerable: false
        };
        Object.defineProperty(propertyName ? target[propertyName] : target, "type", opt);
    };
    if (Array.isArray(data)) {
        assignTo = new Array();
        sType(assignTo, type);
    }
    else if (!(assignTo instanceof type) && (typeof assignTo === "object" || assignTo == null)) {
        assignTo = (typeof type == "function" ? new type() : {});
    }
    for (let _prop in data) {
        /** @type {?} */
        let prop = _prop;
        if (Array.isArray(data)) {
            assignTo[prop] = castToObject(data[prop], ((/** @type {?} */ (assignTo))).type || assignTo.constructor, null);
        }
        else {
            /** @type {?} */
            var cl = function () {
                if (assignTo[prop]) {
                    return assignTo["constructor_" + prop] || assignTo[prop].type || assignTo[prop].constructor;
                }
            }();
            /** @type {?} */
            var tmp = data[prop];
            if (!Array.isArray(tmp)) {
                if (typeof tmp == "object" && cl && tmp != null) {
                    assignTo[prop] = castToObject(data[prop], cl, assignTo[prop]);
                }
                else {
                    assignTo[prop] = tmp;
                }
            }
            else {
                /** @type {?} */
                let arr = [];
                for (let el of tmp) {
                    if (cl != null && !(new cl instanceof Array)) {
                        /** @type {?} */
                        let _el = castToObject(el, cl, null);
                        arr.push(_el);
                    }
                    else
                        arr.push(el);
                }
                assignTo[prop] = arr;
            }
        }
    }
    if (typeof data !== "object" && type != null) {
        return type(data);
    }
    return assignTo;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JqZWN0LnV0aWxzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vb2JqZWN0LXV0aWxzLyIsInNvdXJjZXMiOlsibGliL3V0aWxzL29iamVjdC51dGlscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQ0ksTUFBTSxVQUFVLFFBQVEsQ0FBQyxNQUFNLEVBQUUsUUFBOEIsRUFBRSxLQUFNO0lBQ25FLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO0FBQ25ELENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXVCRCxNQUFNLFVBQVUsS0FBSyxDQUFJLE1BQVcsRUFBRSxJQUFLO0lBQ3ZDLElBQUksTUFBTSxFQUFFOztZQUNKLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDNUMsT0FBTyxZQUFZLENBQUMsR0FBRyxFQUFFLElBQUksSUFBSSxNQUFNLENBQUMsV0FBVyxJQUFJLE1BQU0sQ0FBQyxDQUFDO0tBQ2xFO0lBQ0QsT0FBTyxNQUFNLENBQUM7QUFDbEIsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUNELE1BQU0sVUFBVSxjQUFjLENBQVEsTUFBUyxFQUFFLElBQVksRUFBRSxLQUFVLEVBQUUsV0FBa0M7SUFDekcsSUFBSSxJQUFJLElBQUksSUFBSTtRQUFFLE9BQU8sTUFBTSxDQUFDO0lBQ2hDLElBQUksTUFBTSxJQUFJLElBQUksRUFBRTtRQUNoQixNQUFNLEdBQUcsbUJBQUEsRUFBRSxFQUFPLENBQUM7S0FDdEI7O1FBQ0csS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDOztRQUMzRSxHQUFHLEdBQVEsTUFBTTtJQUVyQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7WUFDL0IsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFFbEIsSUFBSSxHQUFHLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxJQUFJLElBQUksRUFBRTtZQUNqQyxHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUE7U0FDeEM7O1lBRUcsT0FBTyxHQUFHO1lBQ1YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUU7Z0JBQ3RCLE9BQU8sS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksSUFBSSxDQUFDO2FBQ25EO1lBQ0QsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQyxFQUFFO1FBRUgsSUFBSSxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQztZQUNqQyxHQUFHLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2QsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDNUIsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksS0FBSyxFQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDbkUsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNsQjthQUNJO1lBQ0QsSUFBSSxXQUFXLElBQUksS0FBSyxFQUFFO2dCQUN0QixLQUFLLEdBQUcsWUFBWSxDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQzthQUM1QztZQUNELEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFLLENBQUE7U0FDbkI7S0FDSjtJQUVELE9BQU8sTUFBTSxDQUFDO0FBQ2xCLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTBCRCxNQUFNLFVBQVUsY0FBYyxDQUFJLE1BQVcsRUFBRSxJQUFZO0lBQ3ZELElBQUksSUFBSSxJQUFJLElBQUk7UUFBRSxPQUFPLE1BQU0sQ0FBQzs7UUFDNUIsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDOztRQUN6RSxHQUFHLEdBQUcsTUFBTTtJQUNoQixLQUFLLElBQUksQ0FBQyxJQUFJLEtBQUssRUFBRTtRQUNqQixJQUFJLENBQUMsR0FBRztZQUNKLE9BQU8sR0FBRyxDQUFDO1FBRWYsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUNoQjtJQUNELE9BQU8sR0FBRyxDQUFDO0FBQ2YsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBdUNELE1BQU0sVUFBVSxNQUFNLENBQUksT0FBWSxFQUFFLElBQVMsRUFBRSxRQUFZOztRQUN2RCxHQUFHLEdBQVEsUUFBUTtJQUN2QixHQUFHLEdBQUcsWUFBWSxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsUUFBUSxJQUFJLEdBQUcsQ0FBQyxDQUFDO0lBQ25ELE9BQU8sR0FBRyxDQUFDO0FBQ2YsQ0FBQzs7Ozs7Ozs7OztBQVNELE1BQU0sVUFBVSxPQUFPLENBQUksSUFBUyxFQUFFLFlBQWdCO0lBQ2xELE9BQU8sVUFBVSxNQUFXLEVBQUUsWUFBaUI7O1lBRXZDLEdBQUcsR0FBdUI7WUFDMUIsS0FBSyxFQUFFLElBQUk7WUFDWCxVQUFVLEVBQUUsS0FBSztTQUNwQjtRQUVELE1BQU0sQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLGNBQWMsR0FBRyxZQUFZLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFbEUsR0FBRyxHQUFHO1lBQ0YsS0FBSyxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxZQUFZLElBQUksSUFBSSxDQUFDLG1CQUFLLElBQUksRUFBQSxDQUFDLEVBQUU7WUFDaEUsUUFBUSxFQUFFLElBQUk7U0FDakIsQ0FBQTtRQUNELE1BQU0sQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLFlBQVksRUFBRSxHQUFHLENBQUMsQ0FBQztRQUVqRCxHQUFHLEdBQUc7WUFDRixHQUFHLEVBQUU7Z0JBQ0QsT0FBTyxJQUFJLENBQUM7WUFDaEIsQ0FBQztZQUNELFVBQVUsRUFBRSxLQUFLO1NBQ3BCLENBQUE7UUFDRCxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsRUFBRSxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDN0QsQ0FBQyxDQUFBO0FBQ0wsQ0FBQzs7Ozs7Ozs7O0FBUUQsTUFBTSxVQUFVLFlBQVksQ0FBSSxJQUFTLEVBQUUsSUFBUyxFQUFFLFFBQWM7O1FBQzVELEtBQUssR0FBRyxVQUFVLE1BQVcsRUFBRSxJQUFTLEVBQUUsWUFBcUI7O1lBQzNELEdBQUcsR0FBdUI7WUFDMUIsR0FBRyxFQUFFO2dCQUNELE9BQU8sSUFBSSxDQUFDO1lBQ2hCLENBQUM7WUFDRCxVQUFVLEVBQUUsS0FBSztTQUNwQjtRQUNELE1BQU0sQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDckYsQ0FBQztJQUNELElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtRQUNyQixRQUFRLEdBQUcsSUFBSSxLQUFLLEVBQU8sQ0FBQztRQUM1QixLQUFLLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ3pCO1NBQ0ksSUFBSSxDQUFDLENBQUMsUUFBUSxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxRQUFRLEtBQUssUUFBUSxJQUFJLFFBQVEsSUFBSSxJQUFJLENBQUMsRUFBRTtRQUN4RixRQUFRLEdBQUcsQ0FBQyxPQUFPLElBQUksSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQzVEO0lBRUQsS0FBSyxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7O1lBQ2hCLElBQUksR0FBUSxLQUFLO1FBQ3JCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNyQixRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLG1CQUFLLFFBQVEsRUFBQSxDQUFDLENBQUMsSUFBSSxJQUFJLFFBQVEsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDakc7YUFDSTs7Z0JBQ0csRUFBRSxHQUFHO2dCQUNMLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNoQixPQUFPLFFBQVEsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVyxDQUFDO2lCQUMvRjtZQUNMLENBQUMsRUFBRTs7Z0JBQ0MsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFFcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ3JCLElBQUksT0FBTyxHQUFHLElBQUksUUFBUSxJQUFJLEVBQUUsSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFO29CQUM3QyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ2pFO3FCQUNJO29CQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUM7aUJBQ3hCO2FBQ0o7aUJBQ0k7O29CQUNHLEdBQUcsR0FBRyxFQUFFO2dCQUVaLEtBQUssSUFBSSxFQUFFLElBQUksR0FBRyxFQUFFO29CQUNoQixJQUFJLEVBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxZQUFZLEtBQUssQ0FBQyxFQUFFOzs0QkFDdEMsR0FBRyxHQUFHLFlBQVksQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQzt3QkFDcEMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDakI7O3dCQUVHLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQ3BCO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUM7YUFDeEI7U0FDSjtLQUNKO0lBQ0QsSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLElBQUksSUFBSSxJQUFJLElBQUksRUFBRTtRQUMxQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNyQjtJQUNELE9BQU8sUUFBUSxDQUFDO0FBQ3BCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuICAgIGV4cG9ydCBmdW5jdGlvbiB0b1N0cmluZyhvYmplY3QsIHJlcGxhY2VyPzogKGtleSwgdmFsdWUpID0+IGFueSwgc3BhY2U/KSB7XHJcbiAgICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KG9iamVjdCwgcmVwbGFjZXIsIHNwYWNlKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIHVzZWQgdG8gZ2V0IGEgY2xvbmUgb2YgYW4gb2JqZWN0IGFuZCBwcmV2ZW50IGFjY2VzcyBieSByZWYsIFxyXG4gICAgICogaWYgdHlwZSBpcyBzcGVjaWZpZWQgb2JqZWN0IHdpbGwgZXJlZGl0YXRlIGFsIGZ1bmN0aW9uIG9mIHRoZSB0eXBlXHJcbiAgICAgKiBcclxuICAgICAqIGNsYXNzIFBlcnNvbntcclxuICAgICAqICAgcHVibGljIG5hbWU6c3RyaW5nO1xyXG4gICAgICogICBjb25zdHJ1Y3RvcihuYW1lKXtcclxuICAgICAqICAgICAgdGhpcy5uYW1lID0gbmFtZTtcclxuICAgICAqICAgfVxyXG4gICAgICogXHJcbiAgICAgKiAgIGdldE5hbWUoKXtcclxuICAgICAqICAgICAgIHJldHVybiB0aGlzLm5hbWU7XHJcbiAgICAgKiAgIH1cclxuICAgICAqIH1cclxuICAgICAqIFxyXG4gICAgICogbGV0IGpvbiA9IG5ldyBQZXJzb24oJ0pPTicpO1xyXG4gICAgICogbGV0IGFub3RoZXJKb24gPSBjbG9uZTxQZXJzb24+KGpvbixQZXJzb24pO1xyXG4gICAgICogXHJcbiAgICAgKiBAcGFyYW0gb2JqZWN0IHNvdXJjZSBvYmplY3QgdG8gYmUgY2xvbmVkXHJcbiAgICAgKiBAcGFyYW0gdHlwZSB0eXBlIGlmIHlvdSB3YW50IHRvIGNhc3QgdGhlIG9iamVjdCBhbmQgZ2V0IGFsbCBmdW5jdGlvbiBvZiB0aGF0XHJcbiAgICAgKi9cclxuICAgIGV4cG9ydCBmdW5jdGlvbiBjbG9uZTxUPihvYmplY3Q6IGFueSwgdHlwZT8pOiBUIHtcclxuICAgICAgICBpZiAob2JqZWN0KSB7XHJcbiAgICAgICAgICAgIGxldCB0bXAgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KG9iamVjdCkpO1xyXG4gICAgICAgICAgICByZXR1cm4gY2FzdFRvT2JqZWN0KHRtcCwgdHlwZSB8fCBvYmplY3QuY29uc3RydWN0b3IgfHwgT2JqZWN0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG9iamVjdDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIHNldCB0aGUgdmFsdWUgYXQgcGF0aCBvZiBhIG9iamVjdCwgaWYgaXMgZ2l2ZW4gY29uc3RydWN0b3Igb3IgYSB0eXBlLCB0aGUgc2V0dGVkIHZhbHVlIHdpbGwgYmUgY2FzdGVkXHJcbiAgICAgKiBcclxuICAgICAqIGVzXHJcbiAgICAgKiBcclxuICAgICAqIGNsYXNzIENhcntcclxuICAgICAqIFxyXG4gICAgICogICAgIHB1YmxpYyBvd25lcjpQZXJzb247XHJcbiAgICAgKiB9XHJcbiAgICAgKiBcclxuICAgICAqIGNsYXNzIFBlcnNvbntcclxuICAgICAqIFxyXG4gICAgICogICBwdWJsaWMgbmFtZTpzdHJpbmc7XHJcbiAgICAgKiBcclxuICAgICAqICAgY29uc3RydWN0b3IobmFtZSl7XHJcbiAgICAgKiAgICAgIHRoaXMubmFtZSA9IG5hbWU7XHJcbiAgICAgKiAgIH1cclxuICAgICAqIFxyXG4gICAgICogICBnZXROYW1lKCl7XHJcbiAgICAgKiAgICAgICByZXR1cm4gdGhpcy5uYW1lO1xyXG4gICAgICogICB9XHJcbiAgICAgKiB9XHJcbiAgICAgKiBcclxuICAgICAqIGxldCBvYmogPSB7fTtcclxuICAgICAqIFxyXG4gICAgICogU2V0VmFsdWVCeVBhdGgob2JqLCdvd25lcicsbmV3IFBlcnNvbignSk9OJykpXHJcbiAgICAgKiBvYmo9PiB7IG93bmVyIDogeyBuYW1lOidKT04nLCBnZXROYW1lOmZuIH19XHJcbiAgICAgKiBcclxuICAgICAqIEBwYXJhbSBvYmplY3Qgb2JqZWN0IHRoYXQgd2lsbCBiZSBtYW5pcHVsYXRlZFxyXG4gICAgICogQHBhcmFtIHBhdGggcGF0aCBvZiBwcm9wZXJ0eSB0byBhc3NpZ24gIHZhbHVlXHJcbiAgICAgKiBAcGFyYW0gdmFsdWUgXHJcbiAgICAgKiBAcGFyYW0gY29uc3RydWN0b3IgY29uc3RydWN0b3IsIGNsYXNzIHR5cGUsIGZvciBjYXN0IG9iamVjdFxyXG4gICAgICovXHJcbiAgICBleHBvcnQgZnVuY3Rpb24gU2V0VmFsdWVCeVBhdGg8VCwgVDI+KG9iamVjdDogVCwgcGF0aDogc3RyaW5nLCB2YWx1ZTogYW55LCBjb25zdHJ1Y3Rvcj86ICguLi5hcmdzOiBhbnkpID0+IFQyKTogVCB7XHJcbiAgICAgICAgaWYgKHBhdGggPT0gbnVsbCkgcmV0dXJuIG9iamVjdDtcclxuICAgICAgICBpZiAob2JqZWN0ID09IG51bGwpIHtcclxuICAgICAgICAgICAgb2JqZWN0ID0ge30gYXMgYW55O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgX3BhdGggPSBwYXRoLnJlcGxhY2UoL1xcWyhcXHcrKVxcXS9nLCBcIi5bJDFdXCIpLnNwbGl0KFwiLlwiKS5maWx0ZXIoZiA9PiBmICE9IFwiXCIpO1xyXG4gICAgICAgIGxldCB0bXA6IGFueSA9IG9iamVjdDtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBfcGF0aC5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBsZXQga2V5ID0gX3BhdGhbaV07XHJcblxyXG4gICAgICAgICAgICBpZiAoa2V5Lm1hdGNoKC9cXFsoXFx3KylcXF0vZykgIT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAga2V5ID0ga2V5LnJlcGxhY2UoL1xcWyhcXHcrKVxcXS9nLCBcIiQxXCIpXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxldCBpc0FycmF5ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGkgKyAxIDwgX3BhdGgubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF9wYXRoW2kgKyAxXS5tYXRjaCgvXFxbKFxcdyspXFxdL2cpICE9IG51bGw7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH0oKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0bXBba2V5XSAmJiBpICE9IF9wYXRoLmxlbmd0aCAtIDEpXHJcbiAgICAgICAgICAgICAgICB0bXAgPSB0bXBba2V5XTtcclxuICAgICAgICAgICAgZWxzZSBpZiAoaSAhPSBfcGF0aC5sZW5ndGggLSAxKSB7XHJcbiAgICAgICAgICAgICAgICB0bXBba2V5XSA9IGlzQXJyYXkgPyB0bXBba2V5XSB8fCBuZXcgQXJyYXk8YW55PigpIDogdG1wW2tleV0gfHwge307XHJcbiAgICAgICAgICAgICAgICB0bXAgPSB0bXBba2V5XTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlmIChjb25zdHJ1Y3RvciAmJiB2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlID0gY2FzdFRvT2JqZWN0KHZhbHVlLCBjb25zdHJ1Y3Rvcik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0bXBba2V5XSA9IHZhbHVlXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBvYmplY3Q7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBCeSBhIGdpdmVuIG9iamVjdCwgdGhpcyBmdW5jdGlvbiByZXR1cm4gdGhlIHByb3BlcnR5IHNwZWNpZmllZCBieSB0aGUgcGF0aFxyXG4gICAgICogXHJcbiAgICAgKiBsZXQgb2JqID0geyBcclxuICAgICAqICAgICAgYSA6IDIsIFxyXG4gICAgICogICAgICBjIDogW3tcclxuICAgICAqICAgICAgICAgICAgICBhOjJcclxuICAgICAqICAgICAgICAgIH0sMixcclxuICAgICAqICAgICAgICAgIHtjIDogXHJcbiAgICAgKiAgICAgICAgICAgICAgeyBcclxuICAgICAqICAgICAgICAgICAgICAgICAgYyA6IDIgXHJcbiAgICAgKiAgICAgICAgICAgICAgfSBcclxuICAgICAqICAgICAgICAgICBdXHJcbiAgICAgKiAgICAgICAgICB9XHJcbiAgICAgKiBcclxuICAgICAqIGxldCB2YWx1ZSA9IEdldFZhbHVlQnlQYXRoKG9iaiwnYScpO1xyXG4gICAgICogdmFsdWU9PjJcclxuICAgICAqIGxldCB2YWx1ZSA9IEdldFZhbHVlQnlQYXRoKG9iaiwnYycpO1xyXG4gICAgICogdmFsdWUgPT4gWy4uLl1cclxuICAgICAqIGxldCB2YWx1ZSA9IEdldFZhbHVlQnlQYXRoKG9iaiwnY1swXS5hJyk7XHJcbiAgICAgKiB2YWx1ZSA9PiAyXHJcbiAgICAgKiBAcGFyYW0gb2JqZWN0IG9iamVjdCB0aGF0IGNvbnRhaW4gdmFsdWVcclxuICAgICAqIEBwYXJhbSBwYXRoIGZ1bGwgcGF0aCBvZiBvYmplY3RcclxuICAgICAqL1xyXG4gICAgZXhwb3J0IGZ1bmN0aW9uIEdldFZhbHVlQnlQYXRoPFQ+KG9iamVjdDogYW55LCBwYXRoOiBzdHJpbmcpOiBUIHtcclxuICAgICAgICBpZiAocGF0aCA9PSBudWxsKSByZXR1cm4gb2JqZWN0O1xyXG4gICAgICAgIGxldCBfcGF0aCA9IHBhdGgucmVwbGFjZSgvXFxbKFxcdyspXFxdL2csIFwiLiQxXCIpLnNwbGl0KFwiLlwiKS5maWx0ZXIoZiA9PiBmICE9IFwiXCIpO1xyXG4gICAgICAgIGxldCByZXMgPSBvYmplY3Q7XHJcbiAgICAgICAgZm9yIChsZXQgayBvZiBfcGF0aCkge1xyXG4gICAgICAgICAgICBpZiAoIXJlcylcclxuICAgICAgICAgICAgICAgIHJldHVybiByZXM7XHJcblxyXG4gICAgICAgICAgICByZXMgPSByZXNba107XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByZXM7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIC8qKlxyXG4gICAgICogYXNzaWduIGFsbCBwcm9wZXJ0eSBvZiBlbGVtZW50IHRvIGEgcmV0dXJuZWQgb2JqZWN0LCBpZiBhc3NpZ25UbyBpcyBnaXZlblxyXG4gICAgICogcHJvcGVydHkgd291bGQgYmUgc2V0dGVkIHRvIGFzc2lnblRvIG9iamVjdCAgXHJcbiAgICAgKiBcclxuICAgICAqIGVzOlxyXG4gICAgICogXHJcbiAgICAgKiBcclxuICAgICAqIGNsYXNzIFBlcnNvbntcclxuICAgICAqICAgcHVibGljIG5hbWU6c3RyaW5nO1xyXG4gICAgICogICBnZXROYW1lKCl7XHJcbiAgICAgKiAgICAgICByZXR1cm4gdGhpcy5uYW1lO1xyXG4gICAgICogICB9XHJcbiAgICAgKiB9XHJcbiAgICAgKiBcclxuICAgICAqIGxldCBlbGVtZW50ID0geyBuYW1lOidKb24nfTtcclxuICAgICAqIFxyXG4gICAgICogZWxlbWVudCBkb2VzIG5vdCBoYXZlIHByb3BlcnR5IGZ1bmN0aW9uIGdldE5hbWUoKSwgZWxlbWVudC5nZXROYW1lKCkgZ2VuZXJhdGUgZXhjZXB0aW9uXHJcbiAgICAgKiBcclxuICAgICAqIGxldCBwZXJzb24gPSBhc3NpZ248UGVyc29uPihlbGVtZW50LFBlcnNvbik7XHJcbiAgICAgKiBcclxuICAgICAqIHBlcnNvbiB3aWxsIGJlIGFuIG9iamVjdCB0eXBlZCBhcyBQZXJzb24sIGFuZCBnZXROYW1lIHdpbGwgYmUgc2V0dGVkIGNvcnJlY3RseSBhbmQgcmV0dXJuICdKb24nXHJcbiAgICAgKiBcclxuICAgICAqIGVzLjJcclxuICAgICAqIFxyXG4gICAgICogbGV0IGVsZW1lbnQgPSB7IG5hbWU6J0pvbid9O1xyXG4gICAgICogXHJcbiAgICAgKiBsZXQgcGVyc29uOlBlcnNvbjtcclxuICAgICAqIFxyXG4gICAgICogYXNzaWduPFBlcnNvbj4oZWxlbWVudCxQZXJzb24scGVyc29uKTtcclxuICAgICAqIFxyXG4gICAgICogcGVyc29uIHdpbGwgYmUgYW4gb2JqZWN0IHR5cGVkIGFzIFBlcnNvbiwgYW5kIGdldE5hbWUgd2lsbCBiZSBzZXR0ZWQgY29ycmVjdGx5IGFuZCByZXR1cm4gJ0pvbicgICAgXHJcbiAgICAgKiBcclxuICAgICAqIEBwYXJhbSBlbGVtZW50IHNvdXJjZSBvZiBkYXRhXHJcbiAgICAgKiBAcGFyYW0gdHlwZSB0eXBlIG9mIHJlc3VsdCwgdXNlZCB0byBjYXN0IG9iamVjdCByZXR1cm5lZFxyXG4gICAgICogQHBhcmFtIGFzc2lnblRvIHVzZWQgdG8gc2V0IHByb3BlcnR5IG9mIGVsZW1lbnQgdG8gYW5vdGhlciBvYmplY3RcclxuICAgICAqL1xyXG4gICAgZXhwb3J0IGZ1bmN0aW9uIGFzc2lnbjxUPihlbGVtZW50OiBhbnksIHR5cGU6IGFueSwgYXNzaWduVG8/OiBUKTogVCB7XHJcbiAgICAgICAgbGV0IHJlczogYW55ID0gYXNzaWduVG87XHJcbiAgICAgICAgcmVzID0gY2FzdFRvT2JqZWN0KGVsZW1lbnQsIHR5cGUsIGFzc2lnblRvIHx8IHJlcyk7XHJcbiAgICAgICAgcmV0dXJuIHJlcztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIGRlY29yYXRvciB0byBzZXQgYSB0eXBlIG9mIGEgb2JqZWN0IHByb3BlcnR5XHJcbiAgICAgKiB0aGlzIHdvdWxkIGJlIHVzZWZ1bCB0byBjYXN0IG9iamVjdCBlbGVtZW50IG9mIGFuIGFycmF5IGNhdXNlIE9iamVjdC5hc3NpZ24gZG9uJ3Qgd29ya1xyXG4gICAgICogXHJcbiAgICAgKiBAcGFyYW0gdHlwZSB0eXBlIGRlZmluaXRpb24gb2Ygb2JqZWN0XHJcbiAgICAgKiBAcGFyYW0gZGVmYXVsdFZhbHVlIHNldCBhIGRlZmF1bHQgdmFsdWUgdG8gb2JqZWN0XHJcbiAgICAgKi9cclxuICAgIGV4cG9ydCBmdW5jdGlvbiBzZXRUeXBlPFQ+KHR5cGU6IGFueSwgZGVmYXVsdFZhbHVlPzogVCkge1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0OiBhbnksIHByb3BlcnR5TmFtZTogYW55KSB7XHJcblxyXG4gICAgICAgICAgICB2YXIgb3B0OiBQcm9wZXJ0eURlc2NyaXB0b3IgPSB7XHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogdHlwZSxcclxuICAgICAgICAgICAgICAgIGVudW1lcmFibGU6IGZhbHNlXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIFwiY29uc3RydWN0b3JfXCIgKyBwcm9wZXJ0eU5hbWUsIG9wdCk7XHJcblxyXG4gICAgICAgICAgICBvcHQgPSB7XHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogdGFyZ2V0W3Byb3BlcnR5TmFtZV0gfHwgZGVmYXVsdFZhbHVlIHx8IG5ldyAoPGFueT50eXBlKSgpLFxyXG4gICAgICAgICAgICAgICAgd3JpdGFibGU6IHRydWVcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBwcm9wZXJ0eU5hbWUsIG9wdCk7XHJcblxyXG4gICAgICAgICAgICBvcHQgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHlwZTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlbnVtZXJhYmxlOiBmYWxzZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXRbcHJvcGVydHlOYW1lXSwgXCJ0eXBlXCIsIG9wdCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogcmV0dXJuICBhIFQgb2JqZWN0IGNhc3RlZCBmcm9tIHBhcmFtcyB0eXBlXHJcbiAgICAgKiBAcGFyYW0gZGF0YSBvYmplY3QgdG8gY2FzdCAoc291cmNlKVxyXG4gICAgICogQHBhcmFtIHR5cGUgdHlwZW9mIChjb25zdHJ1Y3RvcikgcmVzdWx0XHJcbiAgICAgKiBAcGFyYW0gYXNzaWduVG8gb3B0aW9uYWwsIGlmIHlvdSB3b3VsZCBhc3NpZ24gcmVzdWx0IHRvIGV4aXN0aW5nIG9iamVjdFxyXG4gICAgICovXHJcbiAgICBleHBvcnQgZnVuY3Rpb24gY2FzdFRvT2JqZWN0PFQ+KGRhdGE6IGFueSwgdHlwZTogYW55LCBhc3NpZ25Ubz86IGFueSk6IFQge1xyXG4gICAgICAgIHZhciBzVHlwZSA9IGZ1bmN0aW9uICh0YXJnZXQ6IGFueSwgdHlwZTogYW55LCBwcm9wZXJ0eU5hbWU/OiBzdHJpbmcpIHtcclxuICAgICAgICAgICAgdmFyIG9wdDogUHJvcGVydHlEZXNjcmlwdG9yID0ge1xyXG4gICAgICAgICAgICAgICAgZ2V0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHR5cGU7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZW51bWVyYWJsZTogZmFsc2VcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkocHJvcGVydHlOYW1lID8gdGFyZ2V0W3Byb3BlcnR5TmFtZV0gOiB0YXJnZXQsIFwidHlwZVwiLCBvcHQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShkYXRhKSkge1xyXG4gICAgICAgICAgICBhc3NpZ25UbyA9IG5ldyBBcnJheTxhbnk+KCk7XHJcbiAgICAgICAgICAgIHNUeXBlKGFzc2lnblRvLCB0eXBlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZiAoIShhc3NpZ25UbyBpbnN0YW5jZW9mIHR5cGUpICYmICh0eXBlb2YgYXNzaWduVG8gPT09IFwib2JqZWN0XCIgfHwgYXNzaWduVG8gPT0gbnVsbCkpIHtcclxuICAgICAgICAgICAgYXNzaWduVG8gPSAodHlwZW9mIHR5cGUgPT0gXCJmdW5jdGlvblwiID8gbmV3IHR5cGUoKSA6IHt9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZvciAobGV0IF9wcm9wIGluIGRhdGEpIHtcclxuICAgICAgICAgICAgbGV0IHByb3A6IGFueSA9IF9wcm9wO1xyXG4gICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShkYXRhKSkge1xyXG4gICAgICAgICAgICAgICAgYXNzaWduVG9bcHJvcF0gPSBjYXN0VG9PYmplY3QoZGF0YVtwcm9wXSwgKDxhbnk+YXNzaWduVG8pLnR5cGUgfHwgYXNzaWduVG8uY29uc3RydWN0b3IsIG51bGwpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdmFyIGNsID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChhc3NpZ25Ub1twcm9wXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gYXNzaWduVG9bXCJjb25zdHJ1Y3Rvcl9cIiArIHByb3BdIHx8IGFzc2lnblRvW3Byb3BdLnR5cGUgfHwgYXNzaWduVG9bcHJvcF0uY29uc3RydWN0b3I7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSgpXHJcbiAgICAgICAgICAgICAgICB2YXIgdG1wID0gZGF0YVtwcm9wXVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheSh0bXApKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiB0bXAgPT0gXCJvYmplY3RcIiAmJiBjbCAmJiB0bXAgIT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhc3NpZ25Ub1twcm9wXSA9IGNhc3RUb09iamVjdChkYXRhW3Byb3BdLCBjbCwgYXNzaWduVG9bcHJvcF0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXNzaWduVG9bcHJvcF0gPSB0bXA7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGFyciA9IFtdO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBlbCBvZiB0bXApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNsICE9IG51bGwgJiYgIShuZXcgY2wgaW5zdGFuY2VvZiBBcnJheSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBfZWwgPSBjYXN0VG9PYmplY3QoZWwsIGNsLCBudWxsKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyci5wdXNoKF9lbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJyLnB1c2goZWwpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBhc3NpZ25Ub1twcm9wXSA9IGFycjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodHlwZW9mIGRhdGEgIT09IFwib2JqZWN0XCIgJiYgdHlwZSAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0eXBlKGRhdGEpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gYXNzaWduVG87XHJcbiAgICB9XHJcbiJdfQ==