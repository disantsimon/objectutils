/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} object
 * @param {?=} replacer
 * @param {?=} space
 * @return {?}
 */
function toString(object, replacer, space) {
    return JSON.stringify(object, replacer, space);
}
/**
 * used to get a clone of an object and prevent access by ref,
 * if type is specified object will ereditate al function of the type
 *
 * class Person{
 *   public name:string;
 *   constructor(name){
 *      this.name = name;
 *   }
 *
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let jon = new Person('JON');
 * let anotherJon = clone<Person>(jon,Person);
 *
 * @template T
 * @param {?} object source object to be cloned
 * @param {?=} type type if you want to cast the object and get all function of that
 * @return {?}
 */
function clone(object, type) {
    if (object) {
        /** @type {?} */
        let tmp = JSON.parse(JSON.stringify(object));
        return castToObject(tmp, type || object.constructor || Object);
    }
    return object;
}
/**
 * set the value at path of a object, if is given constructor or a type, the setted value will be casted
 *
 * es
 *
 * class Car{
 *
 *     public owner:Person;
 * }
 *
 * class Person{
 *
 *   public name:string;
 *
 *   constructor(name){
 *      this.name = name;
 *   }
 *
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let obj = {};
 *
 * SetValueByPath(obj,'owner',new Person('JON'))
 * obj=> { owner : { name:'JON', getName:fn }}
 *
 * @template T, T2
 * @param {?} object object that will be manipulated
 * @param {?} path path of property to assign  value
 * @param {?} value
 * @param {?=} constructor constructor, class type, for cast object
 * @return {?}
 */
function SetValueByPath(object, path, value, constructor) {
    if (path == null)
        return object;
    if (object == null) {
        object = (/** @type {?} */ ({}));
    }
    /** @type {?} */
    let _path = path.replace(/\[(\w+)\]/g, ".[$1]").split(".").filter(f => f != "");
    /** @type {?} */
    let tmp = object;
    for (let i = 0; i < _path.length; i++) {
        /** @type {?} */
        let key = _path[i];
        if (key.match(/\[(\w+)\]/g) != null) {
            key = key.replace(/\[(\w+)\]/g, "$1");
        }
        /** @type {?} */
        let isArray = function () {
            if (i + 1 < _path.length) {
                return _path[i + 1].match(/\[(\w+)\]/g) != null;
            }
            return false;
        }();
        if (tmp[key] && i != _path.length - 1)
            tmp = tmp[key];
        else if (i != _path.length - 1) {
            tmp[key] = isArray ? tmp[key] || new Array() : tmp[key] || {};
            tmp = tmp[key];
        }
        else {
            if (constructor && value) {
                value = castToObject(value, constructor);
            }
            tmp[key] = value;
        }
    }
    return object;
}
/**
 * By a given object, this function return the property specified by the path
 *
 * let obj = {
 *      a : 2,
 *      c : [{
 *              a:2
 *          },2,
 *          {c :
 *              {
 *                  c : 2
 *              }
 *           ]
 *          }
 *
 * let value = GetValueByPath(obj,'a');
 * value=>2
 * let value = GetValueByPath(obj,'c');
 * value => [...]
 * let value = GetValueByPath(obj,'c[0].a');
 * value => 2
 * @template T
 * @param {?} object object that contain value
 * @param {?} path full path of object
 * @return {?}
 */
function GetValueByPath(object, path) {
    if (path == null)
        return object;
    /** @type {?} */
    let _path = path.replace(/\[(\w+)\]/g, ".$1").split(".").filter(f => f != "");
    /** @type {?} */
    let res = object;
    for (let k of _path) {
        if (!res)
            return res;
        res = res[k];
    }
    return res;
}
/**
 * assign all property of element to a returned object, if assignTo is given
 * property would be setted to assignTo object
 *
 * es:
 *
 *
 * class Person{
 *   public name:string;
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let element = { name:'Jon'};
 *
 * element does not have property function getName(), element.getName() generate exception
 *
 * let person = assign<Person>(element,Person);
 *
 * person will be an object typed as Person, and getName will be setted correctly and return 'Jon'
 *
 * es.2
 *
 * let element = { name:'Jon'};
 *
 * let person:Person;
 *
 * assign<Person>(element,Person,person);
 *
 * person will be an object typed as Person, and getName will be setted correctly and return 'Jon'
 *
 * @template T
 * @param {?} element source of data
 * @param {?} type type of result, used to cast object returned
 * @param {?=} assignTo used to set property of element to another object
 * @return {?}
 */
function assign(element, type, assignTo) {
    /** @type {?} */
    let res = assignTo;
    res = castToObject(element, type, assignTo || res);
    return res;
}
/**
 * decorator to set a type of a object property
 * this would be useful to cast object element of an array cause Object.assign don't work
 *
 * @template T
 * @param {?} type type definition of object
 * @param {?=} defaultValue set a default value to object
 * @return {?}
 */
function setType(type, defaultValue) {
    return function (target, propertyName) {
        /** @type {?} */
        var opt = {
            value: type,
            enumerable: false
        };
        Object.defineProperty(target, "constructor_" + propertyName, opt);
        opt = {
            value: target[propertyName] || defaultValue || new ((/** @type {?} */ (type)))(),
            writable: true
        };
        Object.defineProperty(target, propertyName, opt);
        opt = {
            get: function () {
                return type;
            },
            enumerable: false
        };
        Object.defineProperty(target[propertyName], "type", opt);
    };
}
/**
 * return  a T object casted from params type
 * @template T
 * @param {?} data object to cast (source)
 * @param {?} type typeof (constructor) result
 * @param {?=} assignTo optional, if you would assign result to existing object
 * @return {?}
 */
function castToObject(data, type, assignTo) {
    /** @type {?} */
    var sType = function (target, type, propertyName) {
        /** @type {?} */
        var opt = {
            get: function () {
                return type;
            },
            enumerable: false
        };
        Object.defineProperty(propertyName ? target[propertyName] : target, "type", opt);
    };
    if (Array.isArray(data)) {
        assignTo = new Array();
        sType(assignTo, type);
    }
    else if (!(assignTo instanceof type) && (typeof assignTo === "object" || assignTo == null)) {
        assignTo = (typeof type == "function" ? new type() : {});
    }
    for (let _prop in data) {
        /** @type {?} */
        let prop = _prop;
        if (Array.isArray(data)) {
            assignTo[prop] = castToObject(data[prop], ((/** @type {?} */ (assignTo))).type || assignTo.constructor, null);
        }
        else {
            /** @type {?} */
            var cl = function () {
                if (assignTo[prop]) {
                    return assignTo["constructor_" + prop] || assignTo[prop].type || assignTo[prop].constructor;
                }
            }();
            /** @type {?} */
            var tmp = data[prop];
            if (!Array.isArray(tmp)) {
                if (typeof tmp == "object" && cl && tmp != null) {
                    assignTo[prop] = castToObject(data[prop], cl, assignTo[prop]);
                }
                else {
                    assignTo[prop] = tmp;
                }
            }
            else {
                /** @type {?} */
                let arr = [];
                for (let el of tmp) {
                    if (cl != null && !(new cl instanceof Array)) {
                        /** @type {?} */
                        let _el = castToObject(el, cl, null);
                        arr.push(_el);
                    }
                    else
                        arr.push(el);
                }
                assignTo[prop] = arr;
            }
        }
    }
    if (typeof data !== "object" && type != null) {
        return type(data);
    }
    return assignTo;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { toString, clone, SetValueByPath, GetValueByPath, assign, setType, castToObject };

//# sourceMappingURL=object-utils.js.map