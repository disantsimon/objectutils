export declare function toString(object: any, replacer?: (key: any, value: any) => any, space?: any): string;
/**
 * used to get a clone of an object and prevent access by ref,
 * if type is specified object will ereditate al function of the type
 *
 * class Person{
 *   public name:string;
 *   constructor(name){
 *      this.name = name;
 *   }
 *
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let jon = new Person('JON');
 * let anotherJon = clone<Person>(jon,Person);
 *
 * @param object source object to be cloned
 * @param type type if you want to cast the object and get all function of that
 */
export declare function clone<T>(object: any, type?: any): T;
/**
 * set the value at path of a object, if is given constructor or a type, the setted value will be casted
 *
 * es
 *
 * class Car{
 *
 *     public owner:Person;
 * }
 *
 * class Person{
 *
 *   public name:string;
 *
 *   constructor(name){
 *      this.name = name;
 *   }
 *
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let obj = {};
 *
 * SetValueByPath(obj,'owner',new Person('JON'))
 * obj=> { owner : { name:'JON', getName:fn }}
 *
 * @param object object that will be manipulated
 * @param path path of property to assign  value
 * @param value
 * @param constructor constructor, class type, for cast object
 */
export declare function SetValueByPath<T, T2>(object: T, path: string, value: any, constructor?: (...args: any) => T2): T;
/**
 * By a given object, this function return the property specified by the path
 *
 * let obj = {
 *      a : 2,
 *      c : [{
 *              a:2
 *          },2,
 *          {c :
 *              {
 *                  c : 2
 *              }
 *           ]
 *          }
 *
 * let value = GetValueByPath(obj,'a');
 * value=>2
 * let value = GetValueByPath(obj,'c');
 * value => [...]
 * let value = GetValueByPath(obj,'c[0].a');
 * value => 2
 * @param object object that contain value
 * @param path full path of object
 */
export declare function GetValueByPath<T>(object: any, path: string): T;
/**
 * assign all property of element to a returned object, if assignTo is given
 * property would be setted to assignTo object
 *
 * es:
 *
 *
 * class Person{
 *   public name:string;
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let element = { name:'Jon'};
 *
 * element does not have property function getName(), element.getName() generate exception
 *
 * let person = assign<Person>(element,Person);
 *
 * person will be an object typed as Person, and getName will be setted correctly and return 'Jon'
 *
 * es.2
 *
 * let element = { name:'Jon'};
 *
 * let person:Person;
 *
 * assign<Person>(element,Person,person);
 *
 * person will be an object typed as Person, and getName will be setted correctly and return 'Jon'
 *
 * @param element source of data
 * @param type type of result, used to cast object returned
 * @param assignTo used to set property of element to another object
 */
export declare function assign<T>(element: any, type: any, assignTo?: T): T;
/**
 * decorator to set a type of a object property
 * this would be useful to cast object element of an array cause Object.assign don't work
 *
 * @param type type definition of object
 * @param defaultValue set a default value to object
 */
export declare function setType<T>(type: any, defaultValue?: T): (target: any, propertyName: any) => void;
/**
 * return  a T object casted from params type
 * @param data object to cast (source)
 * @param type typeof (constructor) result
 * @param assignTo optional, if you would assign result to existing object
 */
export declare function castToObject<T>(data: any, type: any, assignTo?: any): T;
