import { __values } from 'tslib';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} object
 * @param {?=} replacer
 * @param {?=} space
 * @return {?}
 */
function toString(object, replacer, space) {
    return JSON.stringify(object, replacer, space);
}
/**
 * used to get a clone of an object and prevent access by ref,
 * if type is specified object will ereditate al function of the type
 *
 * class Person{
 *   public name:string;
 *   constructor(name){
 *      this.name = name;
 *   }
 *
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let jon = new Person('JON');
 * let anotherJon = clone<Person>(jon,Person);
 *
 * @template T
 * @param {?} object source object to be cloned
 * @param {?=} type type if you want to cast the object and get all function of that
 * @return {?}
 */
function clone(object, type) {
    if (object) {
        /** @type {?} */
        var tmp = JSON.parse(JSON.stringify(object));
        return castToObject(tmp, type || object.constructor || Object);
    }
    return object;
}
/**
 * set the value at path of a object, if is given constructor or a type, the setted value will be casted
 *
 * es
 *
 * class Car{
 *
 *     public owner:Person;
 * }
 *
 * class Person{
 *
 *   public name:string;
 *
 *   constructor(name){
 *      this.name = name;
 *   }
 *
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let obj = {};
 *
 * SetValueByPath(obj,'owner',new Person('JON'))
 * obj=> { owner : { name:'JON', getName:fn }}
 *
 * @template T, T2
 * @param {?} object object that will be manipulated
 * @param {?} path path of property to assign  value
 * @param {?} value
 * @param {?=} constructor constructor, class type, for cast object
 * @return {?}
 */
function SetValueByPath(object, path, value, constructor) {
    if (path == null)
        return object;
    if (object == null) {
        object = (/** @type {?} */ ({}));
    }
    /** @type {?} */
    var _path = path.replace(/\[(\w+)\]/g, ".[$1]").split(".").filter(function (f) { return f != ""; });
    /** @type {?} */
    var tmp = object;
    var _loop_1 = function (i) {
        /** @type {?} */
        var key = _path[i];
        if (key.match(/\[(\w+)\]/g) != null) {
            key = key.replace(/\[(\w+)\]/g, "$1");
        }
        /** @type {?} */
        var isArray = function () {
            if (i + 1 < _path.length) {
                return _path[i + 1].match(/\[(\w+)\]/g) != null;
            }
            return false;
        }();
        if (tmp[key] && i != _path.length - 1)
            tmp = tmp[key];
        else if (i != _path.length - 1) {
            tmp[key] = isArray ? tmp[key] || new Array() : tmp[key] || {};
            tmp = tmp[key];
        }
        else {
            if (constructor && value) {
                value = castToObject(value, constructor);
            }
            tmp[key] = value;
        }
    };
    for (var i = 0; i < _path.length; i++) {
        _loop_1(i);
    }
    return object;
}
/**
 * By a given object, this function return the property specified by the path
 *
 * let obj = {
 *      a : 2,
 *      c : [{
 *              a:2
 *          },2,
 *          {c :
 *              {
 *                  c : 2
 *              }
 *           ]
 *          }
 *
 * let value = GetValueByPath(obj,'a');
 * value=>2
 * let value = GetValueByPath(obj,'c');
 * value => [...]
 * let value = GetValueByPath(obj,'c[0].a');
 * value => 2
 * @template T
 * @param {?} object object that contain value
 * @param {?} path full path of object
 * @return {?}
 */
function GetValueByPath(object, path) {
    var e_1, _a;
    if (path == null)
        return object;
    /** @type {?} */
    var _path = path.replace(/\[(\w+)\]/g, ".$1").split(".").filter(function (f) { return f != ""; });
    /** @type {?} */
    var res = object;
    try {
        for (var _path_1 = __values(_path), _path_1_1 = _path_1.next(); !_path_1_1.done; _path_1_1 = _path_1.next()) {
            var k = _path_1_1.value;
            if (!res)
                return res;
            res = res[k];
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (_path_1_1 && !_path_1_1.done && (_a = _path_1.return)) _a.call(_path_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return res;
}
/**
 * assign all property of element to a returned object, if assignTo is given
 * property would be setted to assignTo object
 *
 * es:
 *
 *
 * class Person{
 *   public name:string;
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let element = { name:'Jon'};
 *
 * element does not have property function getName(), element.getName() generate exception
 *
 * let person = assign<Person>(element,Person);
 *
 * person will be an object typed as Person, and getName will be setted correctly and return 'Jon'
 *
 * es.2
 *
 * let element = { name:'Jon'};
 *
 * let person:Person;
 *
 * assign<Person>(element,Person,person);
 *
 * person will be an object typed as Person, and getName will be setted correctly and return 'Jon'
 *
 * @template T
 * @param {?} element source of data
 * @param {?} type type of result, used to cast object returned
 * @param {?=} assignTo used to set property of element to another object
 * @return {?}
 */
function assign(element, type, assignTo) {
    /** @type {?} */
    var res = assignTo;
    res = castToObject(element, type, assignTo || res);
    return res;
}
/**
 * decorator to set a type of a object property
 * this would be useful to cast object element of an array cause Object.assign don't work
 *
 * @template T
 * @param {?} type type definition of object
 * @param {?=} defaultValue set a default value to object
 * @return {?}
 */
function setType(type, defaultValue) {
    return function (target, propertyName) {
        /** @type {?} */
        var opt = {
            value: type,
            enumerable: false
        };
        Object.defineProperty(target, "constructor_" + propertyName, opt);
        opt = {
            value: target[propertyName] || defaultValue || new ((/** @type {?} */ (type)))(),
            writable: true
        };
        Object.defineProperty(target, propertyName, opt);
        opt = {
            get: function () {
                return type;
            },
            enumerable: false
        };
        Object.defineProperty(target[propertyName], "type", opt);
    };
}
/**
 * return  a T object casted from params type
 * @template T
 * @param {?} data object to cast (source)
 * @param {?} type typeof (constructor) result
 * @param {?=} assignTo optional, if you would assign result to existing object
 * @return {?}
 */
function castToObject(data, type, assignTo) {
    /** @type {?} */
    var sType = function (target, type, propertyName) {
        /** @type {?} */
        var opt = {
            get: function () {
                return type;
            },
            enumerable: false
        };
        Object.defineProperty(propertyName ? target[propertyName] : target, "type", opt);
    };
    if (Array.isArray(data)) {
        assignTo = new Array();
        sType(assignTo, type);
    }
    else if (!(assignTo instanceof type) && (typeof assignTo === "object" || assignTo == null)) {
        assignTo = (typeof type == "function" ? new type() : {});
    }
    var _loop_2 = function (_prop) {
        var e_2, _a;
        /** @type {?} */
        var prop = _prop;
        if (Array.isArray(data)) {
            assignTo[prop] = castToObject(data[prop], ((/** @type {?} */ (assignTo))).type || assignTo.constructor, null);
        }
        else {
            cl = function () {
                if (assignTo[prop]) {
                    return assignTo["constructor_" + prop] || assignTo[prop].type || assignTo[prop].constructor;
                }
            }();
            tmp = data[prop];
            if (!Array.isArray(tmp)) {
                if (typeof tmp == "object" && cl && tmp != null) {
                    assignTo[prop] = castToObject(data[prop], cl, assignTo[prop]);
                }
                else {
                    assignTo[prop] = tmp;
                }
            }
            else {
                /** @type {?} */
                var arr = [];
                try {
                    for (var tmp_1 = __values(tmp), tmp_1_1 = tmp_1.next(); !tmp_1_1.done; tmp_1_1 = tmp_1.next()) {
                        var el = tmp_1_1.value;
                        if (cl != null && !(new cl instanceof Array)) {
                            /** @type {?} */
                            var _el = castToObject(el, cl, null);
                            arr.push(_el);
                        }
                        else
                            arr.push(el);
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (tmp_1_1 && !tmp_1_1.done && (_a = tmp_1.return)) _a.call(tmp_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
                assignTo[prop] = arr;
            }
        }
    };
    var cl, tmp;
    for (var _prop in data) {
        _loop_2(_prop);
    }
    if (typeof data !== "object" && type != null) {
        return type(data);
    }
    return assignTo;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { toString, clone, SetValueByPath, GetValueByPath, assign, setType, castToObject };

//# sourceMappingURL=object-utils.js.map