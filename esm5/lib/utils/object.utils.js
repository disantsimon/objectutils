/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * @param {?} object
 * @param {?=} replacer
 * @param {?=} space
 * @return {?}
 */
export function toString(object, replacer, space) {
    return JSON.stringify(object, replacer, space);
}
/**
 * used to get a clone of an object and prevent access by ref,
 * if type is specified object will ereditate al function of the type
 *
 * class Person{
 *   public name:string;
 *   constructor(name){
 *      this.name = name;
 *   }
 *
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let jon = new Person('JON');
 * let anotherJon = clone<Person>(jon,Person);
 *
 * @template T
 * @param {?} object source object to be cloned
 * @param {?=} type type if you want to cast the object and get all function of that
 * @return {?}
 */
export function clone(object, type) {
    if (object) {
        /** @type {?} */
        var tmp = JSON.parse(JSON.stringify(object));
        return castToObject(tmp, type || object.constructor || Object);
    }
    return object;
}
/**
 * set the value at path of a object, if is given constructor or a type, the setted value will be casted
 *
 * es
 *
 * class Car{
 *
 *     public owner:Person;
 * }
 *
 * class Person{
 *
 *   public name:string;
 *
 *   constructor(name){
 *      this.name = name;
 *   }
 *
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let obj = {};
 *
 * SetValueByPath(obj,'owner',new Person('JON'))
 * obj=> { owner : { name:'JON', getName:fn }}
 *
 * @template T, T2
 * @param {?} object object that will be manipulated
 * @param {?} path path of property to assign  value
 * @param {?} value
 * @param {?=} constructor constructor, class type, for cast object
 * @return {?}
 */
export function SetValueByPath(object, path, value, constructor) {
    if (path == null)
        return object;
    if (object == null) {
        object = (/** @type {?} */ ({}));
    }
    /** @type {?} */
    var _path = path.replace(/\[(\w+)\]/g, ".[$1]").split(".").filter(function (f) { return f != ""; });
    /** @type {?} */
    var tmp = object;
    var _loop_1 = function (i) {
        /** @type {?} */
        var key = _path[i];
        if (key.match(/\[(\w+)\]/g) != null) {
            key = key.replace(/\[(\w+)\]/g, "$1");
        }
        /** @type {?} */
        var isArray = function () {
            if (i + 1 < _path.length) {
                return _path[i + 1].match(/\[(\w+)\]/g) != null;
            }
            return false;
        }();
        if (tmp[key] && i != _path.length - 1)
            tmp = tmp[key];
        else if (i != _path.length - 1) {
            tmp[key] = isArray ? tmp[key] || new Array() : tmp[key] || {};
            tmp = tmp[key];
        }
        else {
            if (constructor && value) {
                value = castToObject(value, constructor);
            }
            tmp[key] = value;
        }
    };
    for (var i = 0; i < _path.length; i++) {
        _loop_1(i);
    }
    return object;
}
/**
 * By a given object, this function return the property specified by the path
 *
 * let obj = {
 *      a : 2,
 *      c : [{
 *              a:2
 *          },2,
 *          {c :
 *              {
 *                  c : 2
 *              }
 *           ]
 *          }
 *
 * let value = GetValueByPath(obj,'a');
 * value=>2
 * let value = GetValueByPath(obj,'c');
 * value => [...]
 * let value = GetValueByPath(obj,'c[0].a');
 * value => 2
 * @template T
 * @param {?} object object that contain value
 * @param {?} path full path of object
 * @return {?}
 */
export function GetValueByPath(object, path) {
    var e_1, _a;
    if (path == null)
        return object;
    /** @type {?} */
    var _path = path.replace(/\[(\w+)\]/g, ".$1").split(".").filter(function (f) { return f != ""; });
    /** @type {?} */
    var res = object;
    try {
        for (var _path_1 = tslib_1.__values(_path), _path_1_1 = _path_1.next(); !_path_1_1.done; _path_1_1 = _path_1.next()) {
            var k = _path_1_1.value;
            if (!res)
                return res;
            res = res[k];
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (_path_1_1 && !_path_1_1.done && (_a = _path_1.return)) _a.call(_path_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return res;
}
/**
 * assign all property of element to a returned object, if assignTo is given
 * property would be setted to assignTo object
 *
 * es:
 *
 *
 * class Person{
 *   public name:string;
 *   getName(){
 *       return this.name;
 *   }
 * }
 *
 * let element = { name:'Jon'};
 *
 * element does not have property function getName(), element.getName() generate exception
 *
 * let person = assign<Person>(element,Person);
 *
 * person will be an object typed as Person, and getName will be setted correctly and return 'Jon'
 *
 * es.2
 *
 * let element = { name:'Jon'};
 *
 * let person:Person;
 *
 * assign<Person>(element,Person,person);
 *
 * person will be an object typed as Person, and getName will be setted correctly and return 'Jon'
 *
 * @template T
 * @param {?} element source of data
 * @param {?} type type of result, used to cast object returned
 * @param {?=} assignTo used to set property of element to another object
 * @return {?}
 */
export function assign(element, type, assignTo) {
    /** @type {?} */
    var res = assignTo;
    res = castToObject(element, type, assignTo || res);
    return res;
}
/**
 * decorator to set a type of a object property
 * this would be useful to cast object element of an array cause Object.assign don't work
 *
 * @template T
 * @param {?} type type definition of object
 * @param {?=} defaultValue set a default value to object
 * @return {?}
 */
export function setType(type, defaultValue) {
    return function (target, propertyName) {
        /** @type {?} */
        var opt = {
            value: type,
            enumerable: false
        };
        Object.defineProperty(target, "constructor_" + propertyName, opt);
        opt = {
            value: target[propertyName] || defaultValue || new ((/** @type {?} */ (type)))(),
            writable: true
        };
        Object.defineProperty(target, propertyName, opt);
        opt = {
            get: function () {
                return type;
            },
            enumerable: false
        };
        Object.defineProperty(target[propertyName], "type", opt);
    };
}
/**
 * return  a T object casted from params type
 * @template T
 * @param {?} data object to cast (source)
 * @param {?} type typeof (constructor) result
 * @param {?=} assignTo optional, if you would assign result to existing object
 * @return {?}
 */
export function castToObject(data, type, assignTo) {
    /** @type {?} */
    var sType = function (target, type, propertyName) {
        /** @type {?} */
        var opt = {
            get: function () {
                return type;
            },
            enumerable: false
        };
        Object.defineProperty(propertyName ? target[propertyName] : target, "type", opt);
    };
    if (Array.isArray(data)) {
        assignTo = new Array();
        sType(assignTo, type);
    }
    else if (!(assignTo instanceof type) && (typeof assignTo === "object" || assignTo == null)) {
        assignTo = (typeof type == "function" ? new type() : {});
    }
    var _loop_2 = function (_prop) {
        var e_2, _a;
        /** @type {?} */
        var prop = _prop;
        if (Array.isArray(data)) {
            assignTo[prop] = castToObject(data[prop], ((/** @type {?} */ (assignTo))).type || assignTo.constructor, null);
        }
        else {
            cl = function () {
                if (assignTo[prop]) {
                    return assignTo["constructor_" + prop] || assignTo[prop].type || assignTo[prop].constructor;
                }
            }();
            tmp = data[prop];
            if (!Array.isArray(tmp)) {
                if (typeof tmp == "object" && cl && tmp != null) {
                    assignTo[prop] = castToObject(data[prop], cl, assignTo[prop]);
                }
                else {
                    assignTo[prop] = tmp;
                }
            }
            else {
                /** @type {?} */
                var arr = [];
                try {
                    for (var tmp_1 = tslib_1.__values(tmp), tmp_1_1 = tmp_1.next(); !tmp_1_1.done; tmp_1_1 = tmp_1.next()) {
                        var el = tmp_1_1.value;
                        if (cl != null && !(new cl instanceof Array)) {
                            /** @type {?} */
                            var _el = castToObject(el, cl, null);
                            arr.push(_el);
                        }
                        else
                            arr.push(el);
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (tmp_1_1 && !tmp_1_1.done && (_a = tmp_1.return)) _a.call(tmp_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
                assignTo[prop] = arr;
            }
        }
    };
    var cl, tmp;
    for (var _prop in data) {
        _loop_2(_prop);
    }
    if (typeof data !== "object" && type != null) {
        return type(data);
    }
    return assignTo;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JqZWN0LnV0aWxzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vb2JqZWN0LXV0aWxzLyIsInNvdXJjZXMiOlsibGliL3V0aWxzL29iamVjdC51dGlscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUNJLE1BQU0sVUFBVSxRQUFRLENBQUMsTUFBTSxFQUFFLFFBQThCLEVBQUUsS0FBTTtJQUNuRSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztBQUNuRCxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF1QkQsTUFBTSxVQUFVLEtBQUssQ0FBSSxNQUFXLEVBQUUsSUFBSztJQUN2QyxJQUFJLE1BQU0sRUFBRTs7WUFDSixHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzVDLE9BQU8sWUFBWSxDQUFDLEdBQUcsRUFBRSxJQUFJLElBQUksTUFBTSxDQUFDLFdBQVcsSUFBSSxNQUFNLENBQUMsQ0FBQztLQUNsRTtJQUNELE9BQU8sTUFBTSxDQUFDO0FBQ2xCLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DRCxNQUFNLFVBQVUsY0FBYyxDQUFRLE1BQVMsRUFBRSxJQUFZLEVBQUUsS0FBVSxFQUFFLFdBQWtDO0lBQ3pHLElBQUksSUFBSSxJQUFJLElBQUk7UUFBRSxPQUFPLE1BQU0sQ0FBQztJQUNoQyxJQUFJLE1BQU0sSUFBSSxJQUFJLEVBQUU7UUFDaEIsTUFBTSxHQUFHLG1CQUFBLEVBQUUsRUFBTyxDQUFDO0tBQ3RCOztRQUNHLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxPQUFPLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxJQUFJLEVBQUUsRUFBUCxDQUFPLENBQUM7O1FBQzNFLEdBQUcsR0FBUSxNQUFNOzRCQUVaLENBQUM7O1lBQ0YsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFFbEIsSUFBSSxHQUFHLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxJQUFJLElBQUksRUFBRTtZQUNqQyxHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUE7U0FDeEM7O1lBRUcsT0FBTyxHQUFHO1lBQ1YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUU7Z0JBQ3RCLE9BQU8sS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksSUFBSSxDQUFDO2FBQ25EO1lBQ0QsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQyxFQUFFO1FBRUgsSUFBSSxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQztZQUNqQyxHQUFHLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2QsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDNUIsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksS0FBSyxFQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDbkUsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNsQjthQUNJO1lBQ0QsSUFBSSxXQUFXLElBQUksS0FBSyxFQUFFO2dCQUN0QixLQUFLLEdBQUcsWUFBWSxDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQzthQUM1QztZQUNELEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFLLENBQUE7U0FDbkI7SUFDTCxDQUFDO0lBMUJELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRTtnQkFBNUIsQ0FBQztLQTBCVDtJQUVELE9BQU8sTUFBTSxDQUFDO0FBQ2xCLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTBCRCxNQUFNLFVBQVUsY0FBYyxDQUFJLE1BQVcsRUFBRSxJQUFZOztJQUN2RCxJQUFJLElBQUksSUFBSSxJQUFJO1FBQUUsT0FBTyxNQUFNLENBQUM7O1FBQzVCLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxJQUFJLEVBQUUsRUFBUCxDQUFPLENBQUM7O1FBQ3pFLEdBQUcsR0FBRyxNQUFNOztRQUNoQixLQUFjLElBQUEsVUFBQSxpQkFBQSxLQUFLLENBQUEsNEJBQUEsK0NBQUU7WUFBaEIsSUFBSSxDQUFDLGtCQUFBO1lBQ04sSUFBSSxDQUFDLEdBQUc7Z0JBQ0osT0FBTyxHQUFHLENBQUM7WUFFZixHQUFHLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2hCOzs7Ozs7Ozs7SUFDRCxPQUFPLEdBQUcsQ0FBQztBQUNmLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXVDRCxNQUFNLFVBQVUsTUFBTSxDQUFJLE9BQVksRUFBRSxJQUFTLEVBQUUsUUFBWTs7UUFDdkQsR0FBRyxHQUFRLFFBQVE7SUFDdkIsR0FBRyxHQUFHLFlBQVksQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLFFBQVEsSUFBSSxHQUFHLENBQUMsQ0FBQztJQUNuRCxPQUFPLEdBQUcsQ0FBQztBQUNmLENBQUM7Ozs7Ozs7Ozs7QUFTRCxNQUFNLFVBQVUsT0FBTyxDQUFJLElBQVMsRUFBRSxZQUFnQjtJQUNsRCxPQUFPLFVBQVUsTUFBVyxFQUFFLFlBQWlCOztZQUV2QyxHQUFHLEdBQXVCO1lBQzFCLEtBQUssRUFBRSxJQUFJO1lBQ1gsVUFBVSxFQUFFLEtBQUs7U0FDcEI7UUFFRCxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxjQUFjLEdBQUcsWUFBWSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRWxFLEdBQUcsR0FBRztZQUNGLEtBQUssRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksWUFBWSxJQUFJLElBQUksQ0FBQyxtQkFBSyxJQUFJLEVBQUEsQ0FBQyxFQUFFO1lBQ2hFLFFBQVEsRUFBRSxJQUFJO1NBQ2pCLENBQUE7UUFDRCxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxZQUFZLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFakQsR0FBRyxHQUFHO1lBQ0YsR0FBRyxFQUFFO2dCQUNELE9BQU8sSUFBSSxDQUFDO1lBQ2hCLENBQUM7WUFDRCxVQUFVLEVBQUUsS0FBSztTQUNwQixDQUFBO1FBQ0QsTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLEVBQUUsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzdELENBQUMsQ0FBQTtBQUNMLENBQUM7Ozs7Ozs7OztBQVFELE1BQU0sVUFBVSxZQUFZLENBQUksSUFBUyxFQUFFLElBQVMsRUFBRSxRQUFjOztRQUM1RCxLQUFLLEdBQUcsVUFBVSxNQUFXLEVBQUUsSUFBUyxFQUFFLFlBQXFCOztZQUMzRCxHQUFHLEdBQXVCO1lBQzFCLEdBQUcsRUFBRTtnQkFDRCxPQUFPLElBQUksQ0FBQztZQUNoQixDQUFDO1lBQ0QsVUFBVSxFQUFFLEtBQUs7U0FDcEI7UUFDRCxNQUFNLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ3JGLENBQUM7SUFDRCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7UUFDckIsUUFBUSxHQUFHLElBQUksS0FBSyxFQUFPLENBQUM7UUFDNUIsS0FBSyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztLQUN6QjtTQUNJLElBQUksQ0FBQyxDQUFDLFFBQVEsWUFBWSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sUUFBUSxLQUFLLFFBQVEsSUFBSSxRQUFRLElBQUksSUFBSSxDQUFDLEVBQUU7UUFDeEYsUUFBUSxHQUFHLENBQUMsT0FBTyxJQUFJLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUM1RDs0QkFFUSxLQUFLOzs7WUFDTixJQUFJLEdBQVEsS0FBSztRQUNyQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDckIsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxtQkFBSyxRQUFRLEVBQUEsQ0FBQyxDQUFDLElBQUksSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ2pHO2FBQ0k7WUFDRyxFQUFFLEdBQUc7Z0JBQ0wsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ2hCLE9BQU8sUUFBUSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxXQUFXLENBQUM7aUJBQy9GO1lBQ0wsQ0FBQyxFQUFFO1lBQ0MsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFFcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ3JCLElBQUksT0FBTyxHQUFHLElBQUksUUFBUSxJQUFJLEVBQUUsSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFO29CQUM3QyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ2pFO3FCQUNJO29CQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUM7aUJBQ3hCO2FBQ0o7aUJBQ0k7O29CQUNHLEdBQUcsR0FBRyxFQUFFOztvQkFFWixLQUFlLElBQUEsUUFBQSxpQkFBQSxHQUFHLENBQUEsd0JBQUEseUNBQUU7d0JBQWYsSUFBSSxFQUFFLGdCQUFBO3dCQUNQLElBQUksRUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFFLFlBQVksS0FBSyxDQUFDLEVBQUU7O2dDQUN0QyxHQUFHLEdBQUcsWUFBWSxDQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDOzRCQUNwQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3lCQUNqQjs7NEJBRUcsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztxQkFDcEI7Ozs7Ozs7OztnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDO2FBQ3hCO1NBQ0o7O1FBNUJPLEVBQUUsRUFLRixHQUFHO0lBWGYsS0FBSyxJQUFJLEtBQUssSUFBSSxJQUFJO2dCQUFiLEtBQUs7S0FtQ2I7SUFDRCxJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFO1FBQzFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3JCO0lBQ0QsT0FBTyxRQUFRLENBQUM7QUFDcEIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4gICAgZXhwb3J0IGZ1bmN0aW9uIHRvU3RyaW5nKG9iamVjdCwgcmVwbGFjZXI/OiAoa2V5LCB2YWx1ZSkgPT4gYW55LCBzcGFjZT8pIHtcclxuICAgICAgICByZXR1cm4gSlNPTi5zdHJpbmdpZnkob2JqZWN0LCByZXBsYWNlciwgc3BhY2UpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogdXNlZCB0byBnZXQgYSBjbG9uZSBvZiBhbiBvYmplY3QgYW5kIHByZXZlbnQgYWNjZXNzIGJ5IHJlZiwgXHJcbiAgICAgKiBpZiB0eXBlIGlzIHNwZWNpZmllZCBvYmplY3Qgd2lsbCBlcmVkaXRhdGUgYWwgZnVuY3Rpb24gb2YgdGhlIHR5cGVcclxuICAgICAqIFxyXG4gICAgICogY2xhc3MgUGVyc29ue1xyXG4gICAgICogICBwdWJsaWMgbmFtZTpzdHJpbmc7XHJcbiAgICAgKiAgIGNvbnN0cnVjdG9yKG5hbWUpe1xyXG4gICAgICogICAgICB0aGlzLm5hbWUgPSBuYW1lO1xyXG4gICAgICogICB9XHJcbiAgICAgKiBcclxuICAgICAqICAgZ2V0TmFtZSgpe1xyXG4gICAgICogICAgICAgcmV0dXJuIHRoaXMubmFtZTtcclxuICAgICAqICAgfVxyXG4gICAgICogfVxyXG4gICAgICogXHJcbiAgICAgKiBsZXQgam9uID0gbmV3IFBlcnNvbignSk9OJyk7XHJcbiAgICAgKiBsZXQgYW5vdGhlckpvbiA9IGNsb25lPFBlcnNvbj4oam9uLFBlcnNvbik7XHJcbiAgICAgKiBcclxuICAgICAqIEBwYXJhbSBvYmplY3Qgc291cmNlIG9iamVjdCB0byBiZSBjbG9uZWRcclxuICAgICAqIEBwYXJhbSB0eXBlIHR5cGUgaWYgeW91IHdhbnQgdG8gY2FzdCB0aGUgb2JqZWN0IGFuZCBnZXQgYWxsIGZ1bmN0aW9uIG9mIHRoYXRcclxuICAgICAqL1xyXG4gICAgZXhwb3J0IGZ1bmN0aW9uIGNsb25lPFQ+KG9iamVjdDogYW55LCB0eXBlPyk6IFQge1xyXG4gICAgICAgIGlmIChvYmplY3QpIHtcclxuICAgICAgICAgICAgbGV0IHRtcCA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkob2JqZWN0KSk7XHJcbiAgICAgICAgICAgIHJldHVybiBjYXN0VG9PYmplY3QodG1wLCB0eXBlIHx8IG9iamVjdC5jb25zdHJ1Y3RvciB8fCBPYmplY3QpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gb2JqZWN0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogc2V0IHRoZSB2YWx1ZSBhdCBwYXRoIG9mIGEgb2JqZWN0LCBpZiBpcyBnaXZlbiBjb25zdHJ1Y3RvciBvciBhIHR5cGUsIHRoZSBzZXR0ZWQgdmFsdWUgd2lsbCBiZSBjYXN0ZWRcclxuICAgICAqIFxyXG4gICAgICogZXNcclxuICAgICAqIFxyXG4gICAgICogY2xhc3MgQ2Fye1xyXG4gICAgICogXHJcbiAgICAgKiAgICAgcHVibGljIG93bmVyOlBlcnNvbjtcclxuICAgICAqIH1cclxuICAgICAqIFxyXG4gICAgICogY2xhc3MgUGVyc29ue1xyXG4gICAgICogXHJcbiAgICAgKiAgIHB1YmxpYyBuYW1lOnN0cmluZztcclxuICAgICAqIFxyXG4gICAgICogICBjb25zdHJ1Y3RvcihuYW1lKXtcclxuICAgICAqICAgICAgdGhpcy5uYW1lID0gbmFtZTtcclxuICAgICAqICAgfVxyXG4gICAgICogXHJcbiAgICAgKiAgIGdldE5hbWUoKXtcclxuICAgICAqICAgICAgIHJldHVybiB0aGlzLm5hbWU7XHJcbiAgICAgKiAgIH1cclxuICAgICAqIH1cclxuICAgICAqIFxyXG4gICAgICogbGV0IG9iaiA9IHt9O1xyXG4gICAgICogXHJcbiAgICAgKiBTZXRWYWx1ZUJ5UGF0aChvYmosJ293bmVyJyxuZXcgUGVyc29uKCdKT04nKSlcclxuICAgICAqIG9iaj0+IHsgb3duZXIgOiB7IG5hbWU6J0pPTicsIGdldE5hbWU6Zm4gfX1cclxuICAgICAqIFxyXG4gICAgICogQHBhcmFtIG9iamVjdCBvYmplY3QgdGhhdCB3aWxsIGJlIG1hbmlwdWxhdGVkXHJcbiAgICAgKiBAcGFyYW0gcGF0aCBwYXRoIG9mIHByb3BlcnR5IHRvIGFzc2lnbiAgdmFsdWVcclxuICAgICAqIEBwYXJhbSB2YWx1ZSBcclxuICAgICAqIEBwYXJhbSBjb25zdHJ1Y3RvciBjb25zdHJ1Y3RvciwgY2xhc3MgdHlwZSwgZm9yIGNhc3Qgb2JqZWN0XHJcbiAgICAgKi9cclxuICAgIGV4cG9ydCBmdW5jdGlvbiBTZXRWYWx1ZUJ5UGF0aDxULCBUMj4ob2JqZWN0OiBULCBwYXRoOiBzdHJpbmcsIHZhbHVlOiBhbnksIGNvbnN0cnVjdG9yPzogKC4uLmFyZ3M6IGFueSkgPT4gVDIpOiBUIHtcclxuICAgICAgICBpZiAocGF0aCA9PSBudWxsKSByZXR1cm4gb2JqZWN0O1xyXG4gICAgICAgIGlmIChvYmplY3QgPT0gbnVsbCkge1xyXG4gICAgICAgICAgICBvYmplY3QgPSB7fSBhcyBhbnk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCBfcGF0aCA9IHBhdGgucmVwbGFjZSgvXFxbKFxcdyspXFxdL2csIFwiLlskMV1cIikuc3BsaXQoXCIuXCIpLmZpbHRlcihmID0+IGYgIT0gXCJcIik7XHJcbiAgICAgICAgbGV0IHRtcDogYW55ID0gb2JqZWN0O1xyXG5cclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IF9wYXRoLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGxldCBrZXkgPSBfcGF0aFtpXTtcclxuXHJcbiAgICAgICAgICAgIGlmIChrZXkubWF0Y2goL1xcWyhcXHcrKVxcXS9nKSAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBrZXkgPSBrZXkucmVwbGFjZSgvXFxbKFxcdyspXFxdL2csIFwiJDFcIilcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGV0IGlzQXJyYXkgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoaSArIDEgPCBfcGF0aC5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3BhdGhbaSArIDFdLm1hdGNoKC9cXFsoXFx3KylcXF0vZykgIT0gbnVsbDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfSgpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRtcFtrZXldICYmIGkgIT0gX3BhdGgubGVuZ3RoIC0gMSlcclxuICAgICAgICAgICAgICAgIHRtcCA9IHRtcFtrZXldO1xyXG4gICAgICAgICAgICBlbHNlIGlmIChpICE9IF9wYXRoLmxlbmd0aCAtIDEpIHtcclxuICAgICAgICAgICAgICAgIHRtcFtrZXldID0gaXNBcnJheSA/IHRtcFtrZXldIHx8IG5ldyBBcnJheTxhbnk+KCkgOiB0bXBba2V5XSB8fCB7fTtcclxuICAgICAgICAgICAgICAgIHRtcCA9IHRtcFtrZXldO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaWYgKGNvbnN0cnVjdG9yICYmIHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBjYXN0VG9PYmplY3QodmFsdWUsIGNvbnN0cnVjdG9yKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRtcFtrZXldID0gdmFsdWVcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG9iamVjdDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEJ5IGEgZ2l2ZW4gb2JqZWN0LCB0aGlzIGZ1bmN0aW9uIHJldHVybiB0aGUgcHJvcGVydHkgc3BlY2lmaWVkIGJ5IHRoZSBwYXRoXHJcbiAgICAgKiBcclxuICAgICAqIGxldCBvYmogPSB7IFxyXG4gICAgICogICAgICBhIDogMiwgXHJcbiAgICAgKiAgICAgIGMgOiBbe1xyXG4gICAgICogICAgICAgICAgICAgIGE6MlxyXG4gICAgICogICAgICAgICAgfSwyLFxyXG4gICAgICogICAgICAgICAge2MgOiBcclxuICAgICAqICAgICAgICAgICAgICB7IFxyXG4gICAgICogICAgICAgICAgICAgICAgICBjIDogMiBcclxuICAgICAqICAgICAgICAgICAgICB9IFxyXG4gICAgICogICAgICAgICAgIF1cclxuICAgICAqICAgICAgICAgIH1cclxuICAgICAqIFxyXG4gICAgICogbGV0IHZhbHVlID0gR2V0VmFsdWVCeVBhdGgob2JqLCdhJyk7XHJcbiAgICAgKiB2YWx1ZT0+MlxyXG4gICAgICogbGV0IHZhbHVlID0gR2V0VmFsdWVCeVBhdGgob2JqLCdjJyk7XHJcbiAgICAgKiB2YWx1ZSA9PiBbLi4uXVxyXG4gICAgICogbGV0IHZhbHVlID0gR2V0VmFsdWVCeVBhdGgob2JqLCdjWzBdLmEnKTtcclxuICAgICAqIHZhbHVlID0+IDJcclxuICAgICAqIEBwYXJhbSBvYmplY3Qgb2JqZWN0IHRoYXQgY29udGFpbiB2YWx1ZVxyXG4gICAgICogQHBhcmFtIHBhdGggZnVsbCBwYXRoIG9mIG9iamVjdFxyXG4gICAgICovXHJcbiAgICBleHBvcnQgZnVuY3Rpb24gR2V0VmFsdWVCeVBhdGg8VD4ob2JqZWN0OiBhbnksIHBhdGg6IHN0cmluZyk6IFQge1xyXG4gICAgICAgIGlmIChwYXRoID09IG51bGwpIHJldHVybiBvYmplY3Q7XHJcbiAgICAgICAgbGV0IF9wYXRoID0gcGF0aC5yZXBsYWNlKC9cXFsoXFx3KylcXF0vZywgXCIuJDFcIikuc3BsaXQoXCIuXCIpLmZpbHRlcihmID0+IGYgIT0gXCJcIik7XHJcbiAgICAgICAgbGV0IHJlcyA9IG9iamVjdDtcclxuICAgICAgICBmb3IgKGxldCBrIG9mIF9wYXRoKSB7XHJcbiAgICAgICAgICAgIGlmICghcmVzKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlcztcclxuXHJcbiAgICAgICAgICAgIHJlcyA9IHJlc1trXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlcztcclxuICAgIH1cclxuXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBhc3NpZ24gYWxsIHByb3BlcnR5IG9mIGVsZW1lbnQgdG8gYSByZXR1cm5lZCBvYmplY3QsIGlmIGFzc2lnblRvIGlzIGdpdmVuXHJcbiAgICAgKiBwcm9wZXJ0eSB3b3VsZCBiZSBzZXR0ZWQgdG8gYXNzaWduVG8gb2JqZWN0ICBcclxuICAgICAqIFxyXG4gICAgICogZXM6XHJcbiAgICAgKiBcclxuICAgICAqIFxyXG4gICAgICogY2xhc3MgUGVyc29ue1xyXG4gICAgICogICBwdWJsaWMgbmFtZTpzdHJpbmc7XHJcbiAgICAgKiAgIGdldE5hbWUoKXtcclxuICAgICAqICAgICAgIHJldHVybiB0aGlzLm5hbWU7XHJcbiAgICAgKiAgIH1cclxuICAgICAqIH1cclxuICAgICAqIFxyXG4gICAgICogbGV0IGVsZW1lbnQgPSB7IG5hbWU6J0pvbid9O1xyXG4gICAgICogXHJcbiAgICAgKiBlbGVtZW50IGRvZXMgbm90IGhhdmUgcHJvcGVydHkgZnVuY3Rpb24gZ2V0TmFtZSgpLCBlbGVtZW50LmdldE5hbWUoKSBnZW5lcmF0ZSBleGNlcHRpb25cclxuICAgICAqIFxyXG4gICAgICogbGV0IHBlcnNvbiA9IGFzc2lnbjxQZXJzb24+KGVsZW1lbnQsUGVyc29uKTtcclxuICAgICAqIFxyXG4gICAgICogcGVyc29uIHdpbGwgYmUgYW4gb2JqZWN0IHR5cGVkIGFzIFBlcnNvbiwgYW5kIGdldE5hbWUgd2lsbCBiZSBzZXR0ZWQgY29ycmVjdGx5IGFuZCByZXR1cm4gJ0pvbidcclxuICAgICAqIFxyXG4gICAgICogZXMuMlxyXG4gICAgICogXHJcbiAgICAgKiBsZXQgZWxlbWVudCA9IHsgbmFtZTonSm9uJ307XHJcbiAgICAgKiBcclxuICAgICAqIGxldCBwZXJzb246UGVyc29uO1xyXG4gICAgICogXHJcbiAgICAgKiBhc3NpZ248UGVyc29uPihlbGVtZW50LFBlcnNvbixwZXJzb24pO1xyXG4gICAgICogXHJcbiAgICAgKiBwZXJzb24gd2lsbCBiZSBhbiBvYmplY3QgdHlwZWQgYXMgUGVyc29uLCBhbmQgZ2V0TmFtZSB3aWxsIGJlIHNldHRlZCBjb3JyZWN0bHkgYW5kIHJldHVybiAnSm9uJyAgICBcclxuICAgICAqIFxyXG4gICAgICogQHBhcmFtIGVsZW1lbnQgc291cmNlIG9mIGRhdGFcclxuICAgICAqIEBwYXJhbSB0eXBlIHR5cGUgb2YgcmVzdWx0LCB1c2VkIHRvIGNhc3Qgb2JqZWN0IHJldHVybmVkXHJcbiAgICAgKiBAcGFyYW0gYXNzaWduVG8gdXNlZCB0byBzZXQgcHJvcGVydHkgb2YgZWxlbWVudCB0byBhbm90aGVyIG9iamVjdFxyXG4gICAgICovXHJcbiAgICBleHBvcnQgZnVuY3Rpb24gYXNzaWduPFQ+KGVsZW1lbnQ6IGFueSwgdHlwZTogYW55LCBhc3NpZ25Ubz86IFQpOiBUIHtcclxuICAgICAgICBsZXQgcmVzOiBhbnkgPSBhc3NpZ25UbztcclxuICAgICAgICByZXMgPSBjYXN0VG9PYmplY3QoZWxlbWVudCwgdHlwZSwgYXNzaWduVG8gfHwgcmVzKTtcclxuICAgICAgICByZXR1cm4gcmVzO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogZGVjb3JhdG9yIHRvIHNldCBhIHR5cGUgb2YgYSBvYmplY3QgcHJvcGVydHlcclxuICAgICAqIHRoaXMgd291bGQgYmUgdXNlZnVsIHRvIGNhc3Qgb2JqZWN0IGVsZW1lbnQgb2YgYW4gYXJyYXkgY2F1c2UgT2JqZWN0LmFzc2lnbiBkb24ndCB3b3JrXHJcbiAgICAgKiBcclxuICAgICAqIEBwYXJhbSB0eXBlIHR5cGUgZGVmaW5pdGlvbiBvZiBvYmplY3RcclxuICAgICAqIEBwYXJhbSBkZWZhdWx0VmFsdWUgc2V0IGEgZGVmYXVsdCB2YWx1ZSB0byBvYmplY3RcclxuICAgICAqL1xyXG4gICAgZXhwb3J0IGZ1bmN0aW9uIHNldFR5cGU8VD4odHlwZTogYW55LCBkZWZhdWx0VmFsdWU/OiBUKSB7XHJcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICh0YXJnZXQ6IGFueSwgcHJvcGVydHlOYW1lOiBhbnkpIHtcclxuXHJcbiAgICAgICAgICAgIHZhciBvcHQ6IFByb3BlcnR5RGVzY3JpcHRvciA9IHtcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB0eXBlLFxyXG4gICAgICAgICAgICAgICAgZW51bWVyYWJsZTogZmFsc2VcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgXCJjb25zdHJ1Y3Rvcl9cIiArIHByb3BlcnR5TmFtZSwgb3B0KTtcclxuXHJcbiAgICAgICAgICAgIG9wdCA9IHtcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB0YXJnZXRbcHJvcGVydHlOYW1lXSB8fCBkZWZhdWx0VmFsdWUgfHwgbmV3ICg8YW55PnR5cGUpKCksXHJcbiAgICAgICAgICAgICAgICB3cml0YWJsZTogdHJ1ZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIHByb3BlcnR5TmFtZSwgb3B0KTtcclxuXHJcbiAgICAgICAgICAgIG9wdCA9IHtcclxuICAgICAgICAgICAgICAgIGdldDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0eXBlO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVudW1lcmFibGU6IGZhbHNlXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldFtwcm9wZXJ0eU5hbWVdLCBcInR5cGVcIiwgb3B0KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiByZXR1cm4gIGEgVCBvYmplY3QgY2FzdGVkIGZyb20gcGFyYW1zIHR5cGVcclxuICAgICAqIEBwYXJhbSBkYXRhIG9iamVjdCB0byBjYXN0IChzb3VyY2UpXHJcbiAgICAgKiBAcGFyYW0gdHlwZSB0eXBlb2YgKGNvbnN0cnVjdG9yKSByZXN1bHRcclxuICAgICAqIEBwYXJhbSBhc3NpZ25UbyBvcHRpb25hbCwgaWYgeW91IHdvdWxkIGFzc2lnbiByZXN1bHQgdG8gZXhpc3Rpbmcgb2JqZWN0XHJcbiAgICAgKi9cclxuICAgIGV4cG9ydCBmdW5jdGlvbiBjYXN0VG9PYmplY3Q8VD4oZGF0YTogYW55LCB0eXBlOiBhbnksIGFzc2lnblRvPzogYW55KTogVCB7XHJcbiAgICAgICAgdmFyIHNUeXBlID0gZnVuY3Rpb24gKHRhcmdldDogYW55LCB0eXBlOiBhbnksIHByb3BlcnR5TmFtZT86IHN0cmluZykge1xyXG4gICAgICAgICAgICB2YXIgb3B0OiBQcm9wZXJ0eURlc2NyaXB0b3IgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHlwZTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlbnVtZXJhYmxlOiBmYWxzZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShwcm9wZXJ0eU5hbWUgPyB0YXJnZXRbcHJvcGVydHlOYW1lXSA6IHRhcmdldCwgXCJ0eXBlXCIsIG9wdCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KGRhdGEpKSB7XHJcbiAgICAgICAgICAgIGFzc2lnblRvID0gbmV3IEFycmF5PGFueT4oKTtcclxuICAgICAgICAgICAgc1R5cGUoYXNzaWduVG8sIHR5cGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmICghKGFzc2lnblRvIGluc3RhbmNlb2YgdHlwZSkgJiYgKHR5cGVvZiBhc3NpZ25UbyA9PT0gXCJvYmplY3RcIiB8fCBhc3NpZ25UbyA9PSBudWxsKSkge1xyXG4gICAgICAgICAgICBhc3NpZ25UbyA9ICh0eXBlb2YgdHlwZSA9PSBcImZ1bmN0aW9uXCIgPyBuZXcgdHlwZSgpIDoge30pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZm9yIChsZXQgX3Byb3AgaW4gZGF0YSkge1xyXG4gICAgICAgICAgICBsZXQgcHJvcDogYW55ID0gX3Byb3A7XHJcbiAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KGRhdGEpKSB7XHJcbiAgICAgICAgICAgICAgICBhc3NpZ25Ub1twcm9wXSA9IGNhc3RUb09iamVjdChkYXRhW3Byb3BdLCAoPGFueT5hc3NpZ25UbykudHlwZSB8fCBhc3NpZ25Uby5jb25zdHJ1Y3RvciwgbnVsbCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgY2wgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGFzc2lnblRvW3Byb3BdKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhc3NpZ25Ub1tcImNvbnN0cnVjdG9yX1wiICsgcHJvcF0gfHwgYXNzaWduVG9bcHJvcF0udHlwZSB8fCBhc3NpZ25Ub1twcm9wXS5jb25zdHJ1Y3RvcjtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KClcclxuICAgICAgICAgICAgICAgIHZhciB0bXAgPSBkYXRhW3Byb3BdXHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHRtcCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHRtcCA9PSBcIm9iamVjdFwiICYmIGNsICYmIHRtcCAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFzc2lnblRvW3Byb3BdID0gY2FzdFRvT2JqZWN0KGRhdGFbcHJvcF0sIGNsLCBhc3NpZ25Ub1twcm9wXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhc3NpZ25Ub1twcm9wXSA9IHRtcDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgYXJyID0gW107XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGVsIG9mIHRtcCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoY2wgIT0gbnVsbCAmJiAhKG5ldyBjbCBpbnN0YW5jZW9mIEFycmF5KSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IF9lbCA9IGNhc3RUb09iamVjdChlbCwgY2wsIG51bGwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJyLnB1c2goX2VsKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcnIucHVzaChlbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGFzc2lnblRvW3Byb3BdID0gYXJyO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0eXBlb2YgZGF0YSAhPT0gXCJvYmplY3RcIiAmJiB0eXBlICE9IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHR5cGUoZGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBhc3NpZ25UbztcclxuICAgIH1cclxuIl19